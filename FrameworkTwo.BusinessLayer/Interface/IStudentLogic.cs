﻿using FrameworkTwo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkTwo.BusinessLayer.Interface
{
    public interface IStudentLogic
    {
        List<StudentModel> GetStudents();
    }
}
