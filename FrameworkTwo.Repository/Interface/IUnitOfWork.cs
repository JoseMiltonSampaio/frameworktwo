﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FrameworkTwo.Domain;

namespace FrameworkTwo.Repository.Interface
{
    public interface IUnitOfWork
    {
        FrameworkTwoContext DbContext { get; }
        int Save();
    }
}
